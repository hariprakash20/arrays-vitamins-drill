const items = require('./3-arrays-vitamins.cjs');

let vitaminGroups = {};

items.map(item=>{
    let vitaminList = item.contains.split(',');
    vitaminList.map(vitamin=>{
        vitamin = vitamin.trim();
        if(vitaminGroups[vitamin]===undefined) vitaminGroups[vitamin] = []; 
        vitaminGroups[vitamin].push(item.name);
    })
    return vitaminGroups;
})

console.log(vitaminGroups);